let React = window.React = require('react');
let reactRender = require('-aek/react/utils/react-render');
let { VBox } = require('-components/layout');
let { AekReactRouter, RouterView } = require('-components/router');
let Container = require('-components/container');

let router = new AekReactRouter();

let CampusPage = require('./pages/campus');
let IndexPage = require('./pages/index');
let BusPage = require('./pages/bus');
let FavouritesPage = require('./pages/favourites');
const TransportStore = require('./scripts/generic-store');


let Screen = React.createClass({

  componentDidMount:function() {
    TransportStore.fetchCampusData(true);
    TransportStore.dispatchLastUpdated();
    TransportStore.on("change", () => {
      this.forceUpdate();
    });
  },


  render:function() {
    return (
      <Container>
        <VBox>
          <RouterView router={ router }>
            <IndexPage path="/"/>
            <CampusPage path="/campus/:name"/>
            <BusPage path="/bus/:campus/:stop/:number"/>
            <FavouritesPage path="/favourites"/>
          </RouterView>
        </VBox>
      </Container>
    );
  }
});

reactRender(<Screen />);
