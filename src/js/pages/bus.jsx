let React = window.React = require('react');
let Page = require('-components/page');
let { VBox, Panel } = require('-components/layout');
let { BannerHeader } = require('-components/header');
let { Listview, Item } = require('-components/listview');
let { BasicSegment } = require('-components/segment');
var Button = require("@ombiel/aek-lib/react/components/button");

const Config = require('../modules/config');
const TransportStore = require('../scripts/generic-store');
const TransportUtils = require('../scripts/generic-utils');

let BusPage = React.createClass({

  getInitialState:function() {
    let ctx = this.props.ctx;
    var line = TransportStore.asMutable(TransportStore.get(ctx.params.campus + ".stops." + ctx.params.stop + "." + ctx.params.number));
    var isInFavourites = false;
    if(TransportStore.getFromFavourites(ctx.params.campus + ".stops." + ctx.params.stop + "." + ctx.params.number) !== null) {
      isInFavourites = true;
    }
    return {
      line:line,
      campus:ctx.params.campus,
      stop:ctx.params.stop,
      number:ctx.params.number,
      isInFavourites:isInFavourites
    };
  },

  removeFromFavourites:function(e) {
    e.preventDefault();
    this.setState({lineLoading:true});
    TransportStore.removeFromFavourites(this.state.campus + ".stops." + this.state.stop + "." + this.state.number);
    if(TransportStore.getFromFavourites(this.state.campus + ".stops." + this.state.stop + "." + this.state.number) === null) {
      this.setState({ isInFavourites:false, lineLoading:false });
    }else{
      this.setState({lineLoading:false, removingError:true});
    }
  },

  addToFavourites:function(e) {
    e.preventDefault();
    this.setState({lineLoading:true});
    TransportStore.addToFavourites(this.state.campus + ".stops." + this.state.stop + "." + this.state.number);
    if(TransportStore.getFromFavourites(this.state.campus + ".stops." + this.state.stop + "." + this.state.number) !== null) {
      this.setState({ isInFavourites:true, lineLoading:false });
    }
    this.setState({lineLoading:false, addingError:true});
  },

  render:function() {
    let ctx = this.props.ctx;
    let data = this.state.line;
    var lineLoading = this.state.lineLoading;

    let items = [];

    if (data.expectedArrival){
      let arrivals = data.expectedArrival.length > 2 ? TransportUtils.mapCustomDate(data.expectedArrival[0], Config.timeFormat) + ", " + TransportUtils.mapCustomDate(data.expectedArrival[1], Config.timeFormat) + ", " + TransportUtils.mapCustomDate(data.expectedArrival[2], Config.timeFormat)
      : data.expectedArrival.length > 1 ? TransportUtils.mapCustomDate(data.expectedArrival[0], Config.timeFormat) + ", " + TransportUtils.mapCustomDate(data.expectedArrival[1], Config.timeFormat)
      : TransportUtils.mapCustomDate(data.expectedArrival[0], Config.timeFormat);
      items.push(<Item basicLabel label="Expected Arrivals" key="arrivals"> {arrivals} </Item>);
    }
    if (data.destination){
      items.push(<Item basicLabel label="Destination" key="destination"> {data.destination} </Item>);
    }
    if (data.direction){
      items.push(<Item basicLabel label="Direction" key="direction"> {data.direction} </Item>);
    }
    if (data.platform){
      items.push(<Item basicLabel label="Platform" key="platform"> {data.platform} </Item>);
    }

    let favouritesButton = null;

    if(TransportStore.isInFavourites(ctx.params.campus + ".stops." + ctx.params.stop + "." + ctx.params.number)) {
      favouritesButton =
      <Button fluid  variation="secondary" loading={ lineLoading } onClick={ this.removeFromFavourites }>{ Config.language.removeFromFavouritesButtonText }</Button>;
    } else {
      favouritesButton =
      <Button fluid  variation="secondary" loading={ lineLoading } onClick={ this.addToFavourites }>{ Config.language.addToFavouritesButtonText }</Button>;
    }

    return (
      <VBox>
        <Page>
          <VBox>
            <BannerHeader key="banner" theme={Config.theme.secondary} icon={Config.busIcon} iconAlign={Config.iconAlign} data-flex={0}>{data.number + " " + Config.language.toText + " " + data.destination}</BannerHeader>
            <Panel>
              <BasicSegment nopadding>
                <Listview uniformLabels>
                  {items}
                </Listview>
              </BasicSegment>
            </Panel>
          </VBox>
        </Page>
        <div data-flex={0} key={ 'footer-key' }>
          <BasicSegment textAlign="center">
            { favouritesButton }
          </BasicSegment>
        </div>
      </VBox>
    );
  }
});

module.exports = BusPage;
