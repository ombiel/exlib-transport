let React = window.React = require('react');
const Config = require('../modules/config');
var {BannerHeader} = require("@ombiel/aek-lib/react/components/header");
const _ = require('-aek/utils');
let Page = require('-components/page');
var { Listview, Item } = require("-components/listview");
let { BasicSegment} = require('-components/segment');
let { VBox, Panel } = require('-components/layout');
const moment = require("moment");
var Button = require("@ombiel/aek-lib/react/components/button");

const TransportStore = require('../scripts/generic-store');
const TransportUtils = require('../scripts/generic-utils');

let CampusPage = React.createClass({

  refreshPage:function(ev) {
    if(ev) {ev.preventDefault();}
    TransportStore.fetchCampusData();
    TransportStore.dispatchLastUpdated();
  },

  render(){
    let ctx = this.props.ctx;
    let campusName = ctx.params.name;
    let data = TransportStore.asMutable(TransportStore.get(campusName));
    let loading = TransportStore.get("campus1.loading").length + TransportStore.get("campus2.loading").length + TransportStore.get("campus3.loading").length;

    let lastUpdated = TransportStore.get('lastUpdated');
    let lastUpdatedText = lastUpdated === null || loading ? Config.language.refreshButtonNAString : moment(lastUpdated).format(Config.language.refreshButtonDateFormat);
    let lastUpdatedSpan = <span style={{ fontWeight:'bold' }}>{ lastUpdatedText }</span>;
    let refreshIcon = loading ? 'refresh loading' : 'refresh';
    let refreshBar = <Button fluid className='refreshButton' size='tiny' icon={ refreshIcon } iconRight iconBox disabled={ loading }
      onClick={ this.refreshPage }>{Config.language.refreshButtonLabel} { lastUpdatedSpan }</Button>;

      // Loop through the stops and for each stop loop though the lines and add them bit by bit
      let stops = [];
      if (data.stops !== undefined){
        _.forEach(data.stops, (stop, stopIndex) =>{
          let icon = Config.campuses[campusName].stops[stopIndex].type === "bus" ? Config.busIcon : Config.campuses[campusName].stops[stopIndex].type === "train" ? Config.trainIcon : null;

          //sort the lines by arrival time
          let sortedLines = [];
          for (var x in stop){
            sortedLines.push([x, stop[x].expectedArrival[0]]);
          }
          sortedLines.sort((a,b) => {
            let sort = 0;
            if(moment(a[1]).isAfter(moment(b[1]))) {sort = 1;}
            if(moment(a[1]).isBefore(moment(b[1]))) {sort = -1;}
            return sort;
          });

          let lines = [];
          for (var line in sortedLines){
            let lineId =
            <div className='box2'>
              {stop[sortedLines[line][0]].number}
            </div>;
            let destinationName =
            <div className='box2'>
              {stop[sortedLines[line][0]].destination}
            </div>;
            let timestampText;
            if (Config.timestampDisplayFormat === "minutes"){
              timestampText = moment(sortedLines[line][1]).diff(moment(), 'minutes') + " " + Config.language.minutesText;
              if(timestampText.indexOf('1 ' + Config.language.minutesText) === 0) {timestampText = timestampText.substring(0, timestampText.length - 1);}
              if(timestampText.indexOf('0 ' + Config.language.minutesText) === 0) {timestampText = Config.language.dueText;}
            } else if (Config.timestampDisplayFormat === "time"){
              timestampText = TransportUtils.mapCustomDate(sortedLines[line][1], Config.timeFormat);
            }
            let timestamp =
            <div className='box2'>
              <div>{ timestampText }</div>
            </div>;
            let stopItem =
            <Item key={ 'bus-stop-' + sortedLines[line][0] } style={{padding:'0'}} href={"#/bus/" + campusName + "/" + stopIndex + "/" + sortedLines[line][0]}>
            { lineId }
            { destinationName }
            { timestamp }
          </Item>;
          lines.push(stopItem);
        }
        stops.push(<BasicSegment nopadding key={stopIndex + "segment"} >
          <BannerHeader key={stopIndex + "banner"} level='3' style={{padding:'0'}} theme={Config.theme.secondary} icon={icon} iconAlign={Config.iconAlign} data-flex={0}>{Config.campuses[campusName].stops[stopIndex].text}</BannerHeader>
          <Listview style={{margin:'0'}} key={stopIndex + "lines"} >
            { lines }
          </Listview>
        </BasicSegment>);
      });
    }

    // I noticed the stops kept appearing in a random order so I sorted them to appear in the same place each time.
    stops.sort((a,b) => {
      let sort = 0;
      if(a.key > b.key) {sort = 1;}
      if(a.key < b.key) {sort = -1;}
      return sort;
    });

    let changeCampusButton = _.keys(Config.campuses).length > 1
    ?
    <Button variation="secondary" href="#/">{Config.language.changeCampusText}</Button>
    :
    <Button variation="secondary" href="#/favourites">{Config.language.goToFavouritesText}</Button>;

    return (
      <VBox>
        <Page>
          <VBox>
            <BannerHeader key={"campus-banner"} level='2' style={{padding:'0'}} theme={Config.theme.secondary} icon={Config.campusIcon} iconAlign={Config.iconAlign} data-flex={0}>{Config.campuses[campusName].text}</BannerHeader>
            <BasicSegment key='refresh' data-flex={0} style={{ borderBottom:'2px solid #DEDEDE'}}>
              { refreshBar }
            </BasicSegment>
            <Panel>
              <BasicSegment nopadding loading={loading.length > 0}>
                { stops }
              </BasicSegment>
            </Panel>
          </VBox>
        </Page>
        <div data-flex={0} key={ 'footer-key' }>
          <BasicSegment textAlign="center">
            { changeCampusButton }
          </BasicSegment>
        </div>
      </VBox>
    );
  }
});

module.exports = CampusPage;
