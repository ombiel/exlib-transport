let React = window.React = require('react');
const Config = require('../modules/config');
var {BannerHeader} = require("@ombiel/aek-lib/react/components/header");
const _ = require('-aek/utils');
let Page = require('-components/page');
var { Listview, Item } = require("-components/listview");
let { BasicSegment} = require('-components/segment');
let { VBox, Panel } = require('-components/layout');
const moment = require("moment");
var Button = require("@ombiel/aek-lib/react/components/button");

const TransportStore = require('../scripts/generic-store');
const TransportUtils = require('../scripts/generic-utils');

let FavouritesPage = React.createClass({

  refreshPage:function(ev) {
    if(ev) {ev.preventDefault();}
    TransportStore.fetchCampusData();
    TransportStore.dispatchLastUpdated();
  },


  render(){
    let favourites = TransportStore.getFavourites();
    favourites.sort();
    let loading = TransportStore.get("campus1.loading").length + TransportStore.get("campus2.loading").length + TransportStore.get("campus3.loading").length;

    let lastUpdated = TransportStore.get('lastUpdated');
    let lastUpdatedText = lastUpdated === null || loading ? Config.language.refreshButtonNAString : moment(lastUpdated).format(Config.language.refreshButtonDateFormat);
    let lastUpdatedSpan = <span style={{ fontWeight:'bold' }}>{ lastUpdatedText }</span>;
    let refreshIcon = loading ? 'refresh loading' : 'refresh';
      let refreshBar = <Button fluid className='refreshButton' size='tiny' icon={ refreshIcon } iconRight iconBox disabled={ loading }
      onClick={ this.refreshPage }>{Config.language.refreshButtonLabel} { lastUpdatedSpan }</Button>;
      let data = [];
      let stops = [];
      let lines = [];
      if (favourites !== null){
        for (var n in favourites){
          data.push(TransportStore.get(`${favourites[n]}`));
        }
        for (var fave in favourites){
          let route = favourites[fave].split(".");
          let icon = Config.campuses[route[0]].stops[route[2]].type === "bus" ? Config.busIcon : Config.campuses[route[0]].stops[route[2]].type === "train" ? Config.trainIcon : null;
          if (data[fave] !== undefined){
            let icon = Config.campuses[route[0]].stops[route[2]].type === "bus" ? Config.busIcon : Config.campuses[route[0]].stops[route[2]].type === "train" ? Config.trainIcon : null;
            let lineId =
            <div className='box2'>
              {data[fave].number}
            </div>;
            let destinationName =
            <div className='box2'>
              {data[fave].destination}
            </div>;
            let timestampText;
            if (Config.timestampDisplayFormat === "minutes"){
              timestampText = moment(data[fave].expectedArrival[0]).diff(moment(), 'minutes') + " " + Config.language.minutesText;
              if(timestampText.indexOf('1 ' + Config.language.minutesText) === 0) {timestampText = timestampText.substring(0, timestampText.length - 1);}
              if(timestampText.indexOf('0 ' + Config.language.minutesText) === 0) {timestampText = Config.language.dueText;}
            } else if (Config.timestampDisplayFormat === "time"){
              timestampText = TransportUtils.mapCustomDate(data[fave].expectedArrival[0], Config.timeFormat);
            }
            let timestamp =
            <div className='box2'>
              <div>{ timestampText }</div>
            </div>;
            let stopItem =
            <Item key={ 'bus-' + route[3] } style={{padding:'0'}} href={"#/bus/" + route[0] + "/" + route[2] + "/" + route[3]}>
            { lineId }
            { destinationName }
            { timestamp }
          </Item>;
          lines.push([stopItem, data[fave].expectedArrival[0]]);
          lines.sort((a,b) => {
            let sort = 0;
            if(moment(a[1]).isAfter(moment(b[1]))) {sort = 1;}
            if(moment(a[1]).isBefore(moment(b[1]))) {sort = -1;}
            return sort;
          });
          if (fave < (favourites.length - 1)){
            let next = fave++;
            if (favourites[fave].substring(0, 20) !== favourites[next].substring(0, 20)) {
              let x = [];
              lines.forEach((item) => {x.push(item[0]);});
              stops.push(<BasicSegment nopadding key={route[0] + "-" + route[2] + "segment"}>
                <BannerHeader key={route[0] + "-" + route[2] + "banner"} level='3' style={{padding:'0'}} theme={Config.theme.secondary} icon={icon} iconAlign={Config.iconAlign} data-flex={0} subtext={Config.campuses[route[0]].stops[route[2]].text}> {Config.campuses[route[0]].text} </BannerHeader>
                <Listview style={{margin:'0'}} key={route[2] + "lines"} >
                  { x }
                </Listview>
              </BasicSegment>);
              lines = [];
            }
          }
          else {
            stops.push(<BasicSegment nopadding key={route[0] + "-" + route[2] + "segment"}>
              <BannerHeader key={route[0] + "-" + route[2] + "banner"} level='3' style={{padding:'0'}} theme={Config.theme.secondary} icon={icon} iconAlign={Config.iconAlign} data-flex={0} subtext={Config.campuses[route[0]].stops[route[2]].text}> {Config.campuses[route[0]].text} </BannerHeader>
              <Listview style={{margin:'0'}} key={route[2] + "lines"} >
                { lines }
              </Listview>
            </BasicSegment>);
          }
        }
      }
    }

    let changeCampusButton = _.keys(Config.campuses).length > 1
    ?
    <Button variation="secondary" href="#/">{Config.language.changeCampusText}</Button>
    :
    <Button variation="secondary" href="#/favourites">{Config.language.goToFavouritesText}</Button>;

    return (
      <VBox>
        <Page>
          <VBox>
            <BannerHeader key={"campus-banner"} level='2' style={{padding:'0'}} theme={Config.theme.secondary} icon={Config.favouritesIcon} iconAlign={Config.iconAlign} data-flex={0}>{Config.language.favouritesText}</BannerHeader>
            <BasicSegment key='refresh' data-flex={0} style={{ borderBottom:'2px solid #DEDEDE'}}>
              { refreshBar }
            </BasicSegment>
            <Panel>
              <BasicSegment nopadding loading={loading > 0}>
                { stops }
              </BasicSegment>
            </Panel>
          </VBox>
        </Page>
        <div data-flex={0} key={ 'footer-key' }>
          <BasicSegment textAlign="center">
            { changeCampusButton }
          </BasicSegment>
        </div>
      </VBox>
    );
  }
});

module.exports = FavouritesPage;
