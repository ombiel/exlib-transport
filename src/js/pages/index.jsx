let React = window.React = require('react');
let Page = require('-components/page');
let { VBox, Panel } = require('-components/layout');
let { Listview } = require('-components/listview');
let { BasicSegment } = require('-components/segment');
var Divider = require("@ombiel/aek-lib/react/components/divider");

const _ = require('-aek/utils');

const Config = require('../modules/config');

let IndexPage = React.createClass({


  render:function() {

    let campuses = _.map(Config.campuses, (campus)=>{
      return {
        text:campus.text,
        icon:Config.campusIcon,
        thumbSize:"small",
        href:"#/campus/" + campus.name
      };
    });

    campuses.unshift({heading:Config.language.selectCampusText});

    let favourites = [{
      text:Config.language.favouritesText,
      icon:Config.favouritesIcon,
      thumbSize:"small",
      href:"#/favourites"
    }];

    return (
      <Page>
        <VBox>
          <Panel>
            <BasicSegment>
              <Listview flush items={campuses}/>
            </BasicSegment>
            <Divider fitted/>
            <BasicSegment >
              <Listview flush items={favourites}/>
            </BasicSegment>
          </Panel>
        </VBox>
      </Page>
    );
  }
});

module.exports = IndexPage;
