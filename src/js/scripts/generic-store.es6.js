let SimpleStore = require('-aek/simple-store');
let request = require('-aek/request');
let moment = require('moment');
let _ = require('-aek/utils');
let Notifier = require('-aek/notifier');
const Config = require('../modules/config');

// these can be disabled by commenting them out of the plugins array in the constructor()
const loggerPlugin = require('-aek/simple-store/plugins/logger');
const localStoragePlugin = require('-aek/simple-store/plugins/localstorage');

// i.e. 'test-project-key', this is defined as a JavaScript string explicitly in the JSON Config object
const storageKey = Config.storageKey;
var AekStorage = require("-aek/storage");
var storage = new AekStorage(storageKey);

class TransportStore extends SimpleStore {
  constructor() {
    super({
      initialState:{
        lastUpdated: null,
        campus1:{
          loading:[]
        },
        campus2:{
          loading:[]
        },
        campus3:{
          loading:[]
        }
      },
      plugins:[
        loggerPlugin(),
        localStoragePlugin(storageKey)
      ]
    });
  }

  // message takes plaintext only; HTML will be rendered as such
  // clear = true is an ES6 default parameter value: you don't need to pass this to the function call, and if you don't, it will default to true
  genericNotifier(title, message, dismissable, level, autoDismiss, clear = true) {
    if(clear) {Notifier.clear();}
    Notifier.add({
      title: title,
      message: message,
      dismissable: dismissable,
      level: level,
      autoDismiss: autoDismiss
    });
  }

  // children allows you to pass HTML to the Notifier to be displayed
  notifierWithChildren(title, children, dismissable, level, autoDismiss, clear = true) {
    if(clear) {Notifier.clear();}
    Notifier.add({
      title: title,
      children: children,
      dismissable: dismissable,
      level: level,
      autoDismiss: autoDismiss
    });
  }

  dispatchLastUpdated() {
    let ctx = this.context({ group:'MAIN', path:'' });
    // moment doesn't serialise well, so store the formatted string which we can use to create new moment instances out of, should we need to
    let now = moment().format();
    ctx.dispatch({
      name: 'lastUpdated',
      extend:{
        lastUpdated: now
      }
    });
  }

  dispatchCampuses(ctx, data, campusIndex, isError, errorMessage, loading, backgroundLoading) {
    ctx.dispatch({
      name: campusIndex,
      error: isError,
      extend:{
        stops: data,
        errorMessage: errorMessage,
        loading: loading,
        $$$backgroundLoading: backgroundLoading
      }
    });
  }

  fetchCampusData(backgroundLoading = false) {
    let data = {};
    _.forEach(Config.campuses, (campus, campusIndex) =>{
      _.forEach(campus.stops, (stop, stopIndex) => {
        let existingLoading = this.asMutable(this.state[campusIndex].loading);
        if (existingLoading.indexOf(stopIndex) < 0) {existingLoading.push(stopIndex);}
        let ctx = this.context({group:'CAMPUSES', path:campusIndex});
        let existingData = this.get('campuses');
        this.dispatchCampuses(ctx, existingData, campusIndex, false, null, existingLoading, backgroundLoading);
        request.action('get-' + stopIndex).end((err, res) => {
          existingData = this.asMutable(this.get(campusIndex));
          existingLoading = this.asMutable(this.get(campusIndex + ".loading"));
          existingLoading.splice(existingLoading.indexOf(stopIndex), 1);
          if (!err && res){
            let body = _.isArray(res.body) ? res.body : null;
            if(body !== null) {
              if (stop.type === 'bus'){
                _.forEach(res.body, (line) => {
                  if (data[stopIndex]){
                    if (data[stopIndex][line.lineId + "-" + line.direction] && data[stopIndex][line.lineId + "-" + line.direction].destination === line.destinationName){
                      if (!data[stopIndex][line.lineId + "-" + line.direction].expectedArrival.includes(line.expectedArrival)){
                        data[stopIndex][line.lineId + "-" + line.direction].expectedArrival.push(line.expectedArrival);
                      }
                    }
                    else{
                      data[stopIndex][line.lineId + "-" + line.direction] = {
                        "number" : `${line.lineName}`,
                        "destination" : `${line.destinationName}`,
                        "expectedArrival" : [`${line.expectedArrival}`],
                        "platform" : null,
                        "direction": line.direction,
                        "route": campusIndex + ".stops." + stopIndex + "." + line.lineId + "-" + line.direction
                      };
                    }
                  }
                  else{
                    data[stopIndex] = {};
                    data[stopIndex][line.lineId + "-" + line.direction] = {
                      "number" : `${line.lineName}`,
                      "destination" : `${line.destinationName}`,
                      "expectedArrival" : [`${line.expectedArrival}`],
                      "platform" : null,
                      "direction": line.direction,
                      "route": campusIndex + ".stops." + stopIndex + "." + line.lineId + "-" + line.direction
                    };
                  }
                });
                _.forEach(data[stopIndex], (line) => {
                  line.expectedArrival.sort((a,b) => {
                    let sort = 0;
                    if(moment(a).isAfter(moment(b))) {sort = 1;}
                    if(moment(a).isBefore(moment(b))) {sort = -1;}
                    return sort;
                  });
                });
              }
              else if (stop.type === 'train'){
                _.forEach(res.body, (line) => {
                  if (data[stopIndex]){
                    if (data[stopIndex] && data[stopIndex][line.lineId + "-" + line.direction] && (data[stopIndex][line.lineId + "-" + line.direction].destination === line.destinationName)){
                      if (!data[stopIndex][line.lineId + "-" + line.direction].expectedArrival.includes(line.expectedArrival)){
                        data[stopIndex][line.lineId + "-" + line.direction].expectedArrival.push(line.expectedArrival);
                      }
                    }
                    else{
                      data[stopIndex][line.lineId + "-" + line.direction] = {
                        "number" : `${line.lineName}`,
                        "destination" : `${line.destinationName}`,
                        "expectedArrival" : [`${line.expectedArrival}`],
                        "platform": `${line.platformName}`,
                        "direction": line.direction,
                        "route": campusIndex + ".stops." + stopIndex + "." + line.lineId + "-" + line.direction
                      };
                    }
                  }
                  else{
                    data[stopIndex] = {};
                    data[stopIndex][line.lineId + "-" + line.direction] = {
                      "number" : `${line.lineName}`,
                      "destination" : `${line.destinationName}`,
                      "expectedArrival" : [`${line.expectedArrival}`],
                      "platform": `${line.platformName}`,
                      "direction": line.direction,
                      "route": campusIndex + ".stops." + stopIndex + "." + line.lineId + "-" + line.direction
                    };
                  }
                });
                _.forEach(data[stopIndex], (line) => {
                  line.expectedArrival.sort((a,b) => {
                    let sort = 0;
                    if(moment(a).isAfter(moment(b))) {sort = 1;}
                    if(moment(a).isBefore(moment(b))) {sort = -1;}
                    return sort;
                  });
                });
              }
              this.dispatchCampuses(ctx, data, campusIndex, false, null, existingLoading, false);
            } else {
              let error = Config.language.noStopDataText;
              this.dispatchCampuses(ctx, existingData, campusIndex, true, error, existingLoading, false);
              this.genericNotifier(Config.language.serviceFailureText, error, true, 'error', 0);
            }
          }
        });
      });
    });
  }

  removeFromFavourites(lineRef) {
    var existingStorage = storage.get('favourites');
    var toBePushed = toBePushed = existingStorage.slice();
    for(var n in toBePushed) {
      if(toBePushed[n] === lineRef) {
        toBePushed.splice(n, 1);
        storage.set('favourites', toBePushed);
        break;
      }
    }
  }

  addToFavourites(lineRef) {
    var existingStorage = storage.get('favourites');
    var toBePushed = [];
    if(existingStorage === null) {
      toBePushed.push(lineRef);
      storage.set('favourites', toBePushed);
    } else {
      toBePushed = existingStorage.slice();
      var found = false;
      for(var n in toBePushed) {
        if(toBePushed[n] === lineRef) {
          found = true;
          break;
        }
      }

      if(!found) {
        toBePushed.push(lineRef);
        storage.set('favourites', toBePushed);
      }
    }
  }

  isInFavourites(lineRef) {
    var response = storage.get('favourites');
    if(response !== null) {
      return response.includes(lineRef);
    }
    return false;
  }

  getFromFavourites(lineRef) {
    var response = storage.get('favourites');
    var line = null;
    if(response !== null) {
      for(var n in response) {
        if(response[n] === lineRef) {
          line = response[n];
          break;
        }
      }
    }

    return line;
  }

  getFavourites(){
    let favs = storage.get('favourites');
    if(favs){
      return storage.get('favourites');
    }else{
      return [];
    }
  }

}


module.exports = new TransportStore();
